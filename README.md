This is a villain so evil, so sinister, so horribly vile that even the utterance of his name strikes fear into the hearts of men. The only safe way to refer to this King of Darkness is simply "Him!"

A VHDL implementation of the SAP-1 CPU architecture describe in the Book "Digital Computer Electronics" by Albert Paul Malvino and Jerald A. Brown.  
