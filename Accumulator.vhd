library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity accumulator is
     port (    clk    : in  std_logic;
               la     : in  std_logic;
               ea     : in  std_logic;
	          input  : in  std_logic_vector(7 downto 0);
               output : out std_logic_vector(7 downto 0);
               outbuff: out std_logic_vector(7 downto 0));
end accumulator;

architecture behavioral of accumulator is
signal tmp: std_logic_vector(7 downto 0);
signal tmpbuff: std_logic_vector(7 downto 0);
begin
     process(clk,la,ea,input)
     begin
          if (clk'event and clk='1') then
               --Output a buffer
               if ea='1' then
                    tmpbuff<=tmp;
               else
                    tmpbuff<="ZZZZZZZZ";
               end if;
               --Output to ALU 
               if la='1' then
                    tmp<=tmp;
               else
                    tmp<=input;
               end if;
          end if;
     end process;
     outbuff<=tmpbuff;
     output<=tmp;
end behavioral;
