library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity MARAndInput is
     Port (    SW_in  : in  std_logic_vector (7 downto 0);
               buff_in: in  std_logic_vector (3 downto 0); 
               btn_in : in  std_logic;
               clk    : in  std_logic;
               Lm     : in  std_logic;
		     prog   : in  std_logic;
               addr   : out std_logic_vector (3 downto 0);
	          data   : out std_logic_vector (7 downto 0);
		     endp   : out std_logic);
end MARAndInput;

architecture Behavioral of MARAndInput is
signal aux   : std_logic_vector(7 downto 0);
signal count : std_logic_vector(3 downto 0) := "0000";
signal flag  : std_logic := '0';
signal prs   : std_logic := '0';
begin

memoria: process(SW_in, prog, btn_in,buff_in,Lm)
begin
          if(prog = '1')then --Programming Mode
		     if(btn_in'event and btn_in = '1')then
			     aux <= SW_in;
			     if(flag = '0')then
				     flag <= '1';
			     else
				     count <= count + '1';
			     end if;
		     end if;
		     prs <= '0';
          else
               prs <= '1';
               if(lm='0') then --Execution Mode
		          count<=buff_in; --addr
               end if;
	     end if;
end process;

addr <= count;
data <= aux;
endp <= prs;

end Behavioral;

