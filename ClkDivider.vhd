library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity clk_div is
     Port (    clk  : in  std_logic;
               eclk : in  std_logic;
               qout : out std_logic);
end clk_div;

architecture Behavioral of clk_div is
signal qaux: std_logic_vector (33 downto 0):="0000000000000000000000000000000000";
begin
     process(clk)
	begin
          if(eclk='1') then
		     if(clk'event and clk='1')then
			     qaux <= qaux + 1;
		     end if;
          else
               qaux<=not(qaux);
          end if;
	end process;
	qout <= qaux(33);
end Behavioral;

